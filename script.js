const openMenu = () => {
    const iconMenu = document.querySelector('.js-burger');
    if(iconMenu) {
        const menuBody = document.querySelector('.js-burger-menu');
        iconMenu.addEventListener('click', () => {
            iconMenu.classList.toggle('active');
            menuBody.classList.toggle('active');
        });
    }
}
openMenu();
const scroll = () => {
    window.onscroll = () => {stickyFun()};
    const header =  document.querySelector('.js-header');
    const start =  document.querySelector('.header');
    const sticky =  header.offsetTop;
    function stickyFun() {
        if(window.pageYOffset > sticky) {
            start.classList.add('fixed');
        } else {
            start.classList.remove('fixed');
        }
    }
}
scroll();